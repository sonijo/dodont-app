function afficher_alert(error){
    error = document.querySelector(error)
    error.style.top = '8%'
}

function close_alert(error){
    setTimeout(() => {
        error = document.querySelector(error)
        error.style.top = '-25%'
    }, 2000)
    
}


function ajouter_tache(){
    let form = document.querySelector(".add_tache form")
    form.addEventListener("submit", (e) => {
        e.preventDefault()
    
        let titre = form["titre"].value
        let contenu = form["contenu"].value
        let date = form["date"].value
        let heure = form['heure'].value
        let tag = form['tag'].value

        let date_actu = new Date().getFullYear() +"-"+ new Date().getMonth() +"-"+ new Date().getDate()

        if(titre != "" || titre != null){
            let idb = indexedDB.open('crud', 1)
            idb.onupgradeneeded = ()=>{
                let res = idb.result;
                res.createObjectStore('tache', {autoIncrement: true})
            }
            idb.onsuccess = ()=>{
                let res = idb.result;
                let tx = res.transaction('tache', 'readwrite')
                let store = tx.objectStore('tache')
                
                let request = store.add(
                    {
                        titre: titre,
                        contenu: contenu,
                        date: date_actu,
                        date_rappel: date,
                        heure_rappel: heure,
                        effectue: 0,
                        tag: tag
                    }
                )

                request.onsuccess = (e)=>{
                    lister()
                    close_form()
                    afficher_alert('.tache_ajoutee')
                    close_alert('.tache_ajoutee')
                    setTimeout(() => location.reload(), 1700)
                }

            }
        }else{
            afficher_alert(".champs_vides")
            close_alert('.champs_vides')
        }
    })
}



function search(id){
    id = parseInt(id)
    console.log(id)
    let tag = {}
    let idb2 = indexedDB.open("crud_tag", 1)
    idb2.onsuccess= ()=> {
        let res2 = idb2.result
        let tx2 = res2.transaction("tag", "readonly")
        let store2 = tx2.objectStore("tag")
        let cursor2 = store2.openCursor()
        cursor2.onsuccess = (e2) => {
            let curRes2 = e2.target.result
            
            if(curRes2){
                if(curRes2.key == id){
                    tag = curRes2.value
                    console.log(tag)
                    return {libelle: tag.libelle,
                        fond: tag.couleur_fond,
                        texte: tag.couleur_texte
                    }
                }
                curRes2.continue()
            }
        }

    }
    
}

function lister(){
    let tag = {}                
    let nb = 0
    let taches = new Array(0)
    let liste_box = document.querySelector(".liste_box")
    liste_box.innerHTML = ""
    let idb = indexedDB.open("crud", 1)
    idb.onsuccess= ()=> {
        let res = idb.result
        let tx = res.transaction("tache", "readonly")
        let store = tx.objectStore("tache")
        let cursor = store.openCursor()

        cursor.onsuccess = (e) => {
            let curRes = e.target.result

            if(curRes){
                if(curRes.value.effectue == 0){
                    // while(search(curRes.value.tag) == undefined){
                    //     tag = search(curRes.value.tag)
                    // }
                    
                    console.log(tag)
                    nb += 1
                    let div = document.createElement("div")
                    div.setAttribute("class", "row")
                    div.setAttribute("id", "row_" + curRes.key)
                    div.innerHTML = `
                        <!-- <div class="check">
                            <input type="checkbox">
                        </div> -->
    
                        <div class="part_1"  onclick="affiche_detail(${curRes.key}); read_one_tache(${curRes.key}); read_one_tache(${curRes.key});">
                            <div class="titre_tache">
                                <h6>${curRes.value.titre} <span class="tag">Ecole</span></h6>
                                <span class="infos_date">
                                ${curRes.value.date_rappel ? `
                                    <span class="rappel">
                                        <i class="fa-solid fa-circle-exclamation"></i>
                                        <span class="date_rappel">${curRes.value.date_rappel}</span> |
                                        <span class="heure_rappel">${curRes.value.heure_rappel}</span> 
                                    </span>` : `` }
                                    <span class="date">${curRes.value.date}</span>
                                </span>
                            </div>
    
                            <p>${curRes.value.contenu}</p>
                            
                        </div>
    
                        <div class="opt">
                            <span class="i valider" onclick="effectuer(${curRes.key}); read_one_tache(${curRes.key}); effectuer_tache()">
                                <i class="fa-solid fa-check"></i>
                            </span>
                            <span class="i modifier" onclick="open_form('.modifier_tache'); read_one_tache(${curRes.key}); add_modification_data()">
                                <i class="fa-regular fa-pen-to-square"></i>
                            </span>
                            <span class="i supprimer" onclick="open_form('.supprimer_tache'); read_one_tache(${curRes.key})">
                                <i class="fa-solid fa-trash-can"></i>
                            </span>
                            
                        </div>
                    `
                    liste_box.append(div)
                }

                curRes.continue()
            }
            document.getElementById("total").textContent = nb
        }
        localStorage.setItem('taches', JSON.stringify(taches))
    }
}



function read_one_tache(key){
    let idb = indexedDB.open('crud', 1)
    idb.onsuccess = ()=>{
        let res = idb.result
        let tx = res.transaction('tache', 'readonly')
        let store = tx.objectStore('tache')
        let cursor = store.openCursor()
        cursor.onsuccess = () => {
            let curRes = cursor.result
            if(curRes.key == key){
                localStorage.setItem("tache", JSON.stringify({key: curRes.key, data: curRes.value}))
                localStorage.setItem("tache", JSON.stringify({key: curRes.key, data: curRes.value}))
                return true
            }
            curRes.continue()
            
        }
    }
}



function effectuer_tache(){
    let data = JSON.parse(localStorage.getItem("tache"))
    let tache = data.data
    
    let newTache = {
        titre: tache.titre,
        contenu: tache.contenu,
        date_rappel: tache.date_rappel,
        heure_rappel: tache.heure_rappel,
        date: tache.date,
        effectue: 1,
        tag: tache.tag
    }

    let idb = indexedDB.open('crud', 1)
    idb.onsuccess = () => {
        let res = idb.result;
        let tx = res.transaction('tache', 'readwrite')
        let store = tx.objectStore('tache')
        store.put(newTache, data.key);
        
        close_form()
        afficher_alert(".tache_effectuee")
        close_alert(".tache_effectuee")
        setTimeout(() => lister(), 800)
    }
}

function supprimer_tache() {
    let e = JSON.parse(localStorage.getItem("tache")).key
    let idb = indexedDB.open('crud', 1)
    idb.onsuccess = () => {
        let res = idb.result;
        let tx = res.transaction('tache', 'readwrite')
        let store = tx.objectStore('tache')
        store.delete(e)
        setTimeout(()=>lister(), 800)
        lister()
    }
    afficher_alert(".tache_supprimee")
    close_alert(".tache_supprimee")
    close_form()
}


function modifier_tache(){
    let data = JSON.parse(localStorage.getItem("tache"))
    let tache = data.data
    let form = document.querySelector(".modifier_tache form")
    form.addEventListener("submit", (e)=>{
        e.preventDefault()
        let newTache = {
            titre: form["titre"].value,
            contenu: form['contenu'].value,
            date_rappel: form['date'].value,
            heure_rappel: form['heure'].value,
            date: tache.date,
            effectue: tache.effectue,
            tag: form["tag"].value
        }
    
        let idb = indexedDB.open('crud', 1)
        idb.onsuccess = () => {
            let res = idb.result;
            let tx = res.transaction('tache', 'readwrite')
            let store = tx.objectStore('tache')
            store.put(newTache, data.key);
            
            close_form()
            afficher_alert(".tache_modifiee")
            close_alert(".tache_modifiee")
            lister()
        }
    
        
    })

    
}


function lister_tags(box){
    let tags = document.querySelector(box)
    tags.innerHTML = ""

    let idb = indexedDB.open("crud_tag", 1)

    idb.onsuccess= ()=> {
        let res = idb.result
        let tx = res.transaction("tag", "readonly")
        let store = tx.objectStore("tag")
        let cursor = store.openCursor()
        tags.innerHTML = '<option selected>Choisir un tag</option>'
        cursor.onsuccess = (e) => {
            let curRes = e.target.result

            if(curRes){
                let div = document.createElement("option")
                div.setAttribute("value", curRes.key)
                div.innerHTML = `${curRes.value.libelle}`
                tags.append(div)
                
                curRes.continue()
            }
        }
    }
}



function rechercher(chaine){
    let idb = indexedDB.open("crud", 1)
    let liste_box = document.querySelector(".liste_box")
    liste_box.innerHTML = ""
    console.log(chaine)

    idb.onsuccess= ()=> {
        let res = idb.result
        let tx = res.transaction("tache", "readonly")
        let store = tx.objectStore("tache")
        let cursor = store.openCursor()
        cursor.onsuccess = (e) => {
            let curRes = e.target.result

            if(curRes){
                // let titre = JSON.stringify(curRes.value.titre)
                if((curRes.value.titre.toUpperCase().indexOf(chaine.toUpperCase()) > -1 || curRes.value.contenu.toUpperCase().indexOf(chaine.toUpperCase()) > -1) && curRes.value.effectue == 0){
                    let div = document.createElement("div")
                    div.setAttribute("class", "row")
                    div.setAttribute("id", "row_" + curRes.key)
                    div.innerHTML = `
                        <!-- <div class="check">
                            <input type="checkbox">
                        </div> -->
    
                        <div class="part_1"  onclick="affiche_detail(${curRes.key}); read_one_tache(${curRes.key});">
                            <div class="titre_tache">
                                <h6>${curRes.value.titre} <span class="tag">Ecole</span></h6>
                                <span class="infos_date">
                                ${curRes.value.date_rappel ? `
                                    <span class="rappel">
                                        <i class="fa-solid fa-circle-exclamation"></i>
                                        <span class="date_rappel">${curRes.value.date_rappel}</span> |
                                        <span class="heure_rappel">${curRes.value.heure_rappel}</span> 
                                    </span>` : `` }
                                    <span class="date">${curRes.value.date}</span>
                                </span>
                            </div>
    
                            <p>${curRes.value.contenu}</p>
                            
                        </div>
    
                        <div class="opt">
                            <span class="i valider" onclick="effectuer(${curRes.key}); read_one_tache(${curRes.key}); effectuer_tache()">
                                <i class="fa-solid fa-check"></i>
                            </span>
                            <span class="i modifier" onclick="open_form('.modifier_tache'); read_one_tache(${curRes.key}); add_modification_data()">
                                <i class="fa-regular fa-pen-to-square"></i>
                            </span>
                            <span class="i supprimer" onclick="open_form('.supprimer_tache'); read_one_tache(${curRes.key})">
                                <i class="fa-solid fa-trash-can"></i>
                            </span>
                            
                        </div>
                    `
                    liste_box.append(div)
                }
                
                curRes.continue()
            }
        }
    }
}





window.onload = () => {
    lister()
    lister_tags(".add_tache .tag_select")
    lister_tags(".modifier_tache .tag_select")

    document.querySelector(".search_input input").addEventListener("input", () => {
        let chaine = document.querySelector('.search_input input').value
        console.log(chaine)
        rechercher(chaine)
    })
}


