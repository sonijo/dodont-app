function supprimer(e) {
    let idb = indexedDB.open('crud', 2)
    idb.onsuccess = () => {
        let res = idb.result;
        let tx = res.transaction('data', 'readwrite')
        let store = tx.objectStore('data')
        store.delete(e)
        alert("voulez-vous vraiment supprimer cette tag ?")
        location.reload()
    }
    close_form()
    setTimeout(() => {
        afficher_alert(".tache_supprimée")
    }, 300)
}
