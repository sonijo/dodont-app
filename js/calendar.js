window.onload = () => {

    let currentMonth = new Date().getMonth();
    let currentYear = new Date().getFullYear();
    let selectedDay = null;

    function renderCalendar() {
        let daysInMonth = getDaysInMonth(currentMonth, currentYear);
        let daysHTML = '';

        let dayNames = ['Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam', 'Dim'];
        for (let i = 0; i < dayNames.length; i++) {
            daysHTML += `<div class="day">${dayNames[i]}</div>`;
        }

        let firstDay = (new Date(currentYear, currentMonth)).getDay();

        for (let i = 1; i <= daysInMonth; i++) {
            daysHTML += `<div class="day" data-day="${i}">${i}</div>`;
        }

        document.getElementById('days').innerHTML = daysHTML;
        document.getElementById('monthYear').innerHTML = getMonthName(currentMonth) + ' ' + currentYear;

        if (selectedDay !== null) {
            let selectedDayElement = document.querySelector(`.day[data-day="${selectedDay}"]`);
            if (selectedDayElement) {
                selectedDayElement.classList.add('selected');
                updateDateInfo();
            }
        }
    }

    function getDaysInMonth(month, year) {
        return new Date(year, month + 1, 0).getDate();
    }

    function getMonthName(month) {
        let months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
        return months[month];
    }

    function updateDateInfo() {
        let dateInfo = document.getElementById('date-info');
        if (selectedDay !== null) {
            dateInfo.textContent = `${selectedDay} ${getMonthName(currentMonth)} ${currentYear}`;
        } else {
            dateInfo.textContent = '';
        }
        select_tache()
    }

    document.getElementById('prevBtn').addEventListener('click', () => {
        currentMonth--;
        if (currentMonth < 0) {
            currentMonth = 11;
            currentYear--;
        }
        renderCalendar();
    });

    document.getElementById('nextBtn').addEventListener('click', () => {
        currentMonth++;
        if (currentMonth > 11) {
            currentMonth = 0;
            currentYear++;
        }
        renderCalendar();
    });

    document.getElementById('days').addEventListener('click', (event) => {
        if (event.target.classList.contains('day')) {
            let previousDay = document.querySelector('.day.selected');
            if (previousDay) {
                previousDay.classList.remove('selected');
            }
            event.target.classList.add('selected');
            selectedDay = parseInt(event.target.dataset.day);
            updateDateInfo();
        }
    });

    renderCalendar();
   

    function select_tache(){
        let idb = indexedDB.open("crud", 1)
        let liste_box = document.querySelector(".liste_box")
        liste_box.innerHTML = ""

        idb.onsuccess= ()=> {
            let res = idb.result
            let tx = res.transaction("tache", "readonly")
            let store = tx.objectStore("tache")
            let cursor = store.openCursor()
            cursor.onsuccess = (e) => {
                let curRes = e.target.result

                if(curRes){
                    let tache_date = curRes.value.date
                    let date = currentYear+"-"+currentMonth+"-"+selectedDay
                    // if(selectedDay == parseInt(tab[2]) && currentMonth == (parseInt(tab[1]) +1) && currentYear == parseInt(tab[0])){
                    if(date == tache_date && curRes.value.effectue == 0){
                        let div = document.createElement("div")
                        div.setAttribute("class", "row")
                        div.setAttribute("id", "row_" + curRes.key)
                        div.innerHTML = `
                            <!-- <div class="check">
                                <input type="checkbox">
                            </div> -->
        
                            <div class="part_1"  onclick="affiche_detail(${curRes.key}); read_one_tache(${curRes.key});">
                                <div class="titre_tache">
                                    <h6>${curRes.value.titre} <span class="tag">Ecole</span></h6>
                                    <span class="infos_date">
                                    ${curRes.value.date_rappel ? `
                                        <span class="rappel">
                                            <i class="fa-solid fa-circle-exclamation"></i>
                                            <span class="date_rappel">${curRes.value.date_rappel}</span> |
                                            <span class="heure_rappel">${curRes.value.heure_rappel}</span> 
                                        </span>` : `` }
                                        <span class="date">${curRes.value.date}</span>
                                    </span>
                                </div>
        
                                <p>${curRes.value.contenu}</p>
                                
                            </div>
        
                            <div class="opt">
                                <span class="i valider" onclick="effectuer(${curRes.key}); read_one_tache(${curRes.key}); effectuer_tache()">
                                    <i class="fa-solid fa-check"></i>
                                </span>
                                <span class="i modifier" onclick="open_form('.modifier_tache'); read_one_tache(${curRes.key}); add_modification_data()">
                                    <i class="fa-regular fa-pen-to-square"></i>
                                </span>
                                <span class="i supprimer" onclick="open_form('.supprimer_tache'); read_one_tache(${curRes.key})">
                                    <i class="fa-solid fa-trash-can"></i>
                                </span>
                                
                            </div>
                        `
                        liste_box.append(div)
                    }
                    
                    curRes.continue()
                }
            }
        }
    }
   

}