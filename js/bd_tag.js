function afficher_alert(error){
    error = document.querySelector(error)
    error.style.top = '10%'
}

function close_alert(error){
    setTimeout(() => {
        error = document.querySelector(error)
        error.style.top = '-25%'
    }, 2000) 
}

function close_form(){
    let bg = document.querySelector(".bg_flou")
    let boxes = document.querySelectorAll(".box-form")
    bg.setAttribute("style", "display: none")
    for(let i=0; i< boxes.length; i++){
        boxes[i].style.scale = 0.1
        boxes[i].style.top = "-40%"
        boxes[i].style.left = "40%"
    }
}


function lister_tag(){
    let nb = 0
    let grille = document.querySelector(".grille")
    grille.innerHTML = ""
    let idb = indexedDB.open("crud_tag", 1)

    idb.onsuccess= ()=> {
        let res = idb.result
        let tx = res.transaction("tag", "readonly")
        let store = tx.objectStore("tag")
        let cursor = store.openCursor()
        cursor.onsuccess = (e) => {
            let curRes = e.target.result

            if(curRes){
                nb += 1
                let div = document.createElement("div")
                div.setAttribute("class", "box")
                div.setAttribute("id", "box_" + curRes.key)
                // div.style.backgroundColor = curRes.value.couleur_fond
                // div.style.color = curRes.value.couleur_texte
                div.innerHTML = `
                    <div class="img">
                        <div class="image" style="background-color: ${curRes.value.couleur_fond}"></div>
                    </div>
                    <div class="libelle">
                        <span>${curRes.value.libelle}</span>
                        
                    </div>

                    <div class="opt">
                        <span onclick="open_form('.modifier_tag'); read_one_tag(${curRes.key})">
                            <i class="fa-regular fa-pen-to-square"></i>
                        </span>
                        <span onclick="open_form('.supprimer_tag'); read_one_tag(${curRes.key})">
                            <i class="fa-solid fa-trash-can"></i>
                        </span>
                    </div>`

                grille.append(div)
                curRes.continue()
            }
        }
        console.log(nb)
        // document.querySelector(".number").textContent = nb
    }
}


function ajouter_tag(){
    let form = document.querySelector('.add_tag form')
    form.addEventListener("submit", (e) => {
        e.preventDefault()
        let libelle = form['libelle'].value
        let couleur_fond = form['couleur_fond'].value
        let couleur_texte = form['couleur_texte'].value

        console.log(couleur_texte)
        
        if(libelle != null || libelle != ""){
            let idb = indexedDB.open('crud_tag', 1)
            idb.onupgradeneeded = ()=>{
                let res = idb.result;
                res.createObjectStore('tag', {autoIncrement: true})
            }
            idb.onsuccess = (e) => {
                let res = idb.result;
                let tx = res.transaction('tag', 'readwrite')
                let store = tx.objectStore('tag')
                let request = store.add(
                    {
                        libelle: libelle,
                        couleur_fond: couleur_fond,
                        couleur_texte: couleur_texte
                    }
                )
                request.onsuccess = (e)=>{
                    afficher_alert('.tag_ajoute')
                    close_alert('tag_ajoute')
                    close_form()
                    lister_tag()
                }
            }
        }
    })
}



function read_one_tag(key){
    let idb = indexedDB.open('crud_tag', 1)
    idb.onsuccess = ()=>{
        let res = idb.result
        let tx = res.transaction('tag', 'readonly')
        let store = tx.objectStore('tag')
        let cursor = store.openCursor()
        cursor.onsuccess = () => {
            let curRes = cursor.result
            if(curRes.key == key){
                localStorage.setItem("tag", JSON.stringify({key: curRes.key, data: curRes.value}))
                return true
            }
            curRes.continue()
        }
    }
}

function modifier_tag(){
    let data = JSON.parse(localStorage.getItem("tag"))
    let tag = data.data
    let form = document.querySelector(".modifier_tag form")
    form.addEventListener("submit", (e)=>{
        e.preventDefault()
        let newTag = {
            libelle: form['libelle'].value,
            couleur_fond: form['couleur_fond'].value,
            couleur_texte: form['couleur_texte'].value
        }
    
        let idb = indexedDB.open('crud_tag', 1)
        idb.onsuccess = () => {
            let res = idb.result;
            let tx = res.transaction('tag', 'readwrite')
            let store = tx.objectStore('tag')
            store.put(newTag, data.key);
            
            close_form()
            afficher_alert(".tag_modifie")
            close_alert(".tag_modifie")
            lister()
        }
    
        
    })
}


function supprimer_tag(){
    let e = JSON.parse(localStorage.getItem("tag")).key
    let idb = indexedDB.open('crud_tag', 1)
    idb.onsuccess = () => {
        let res = idb.result;
        let tx = res.transaction('tag', 'readwrite')
        let store = tx.objectStore('tag')
        store.delete(e)
        setTimeout(()=>lister_tag(), 800)
        lister()
    }
    afficher_alert(".tag_supprime")
    close_alert(".tag_supprime")
    close_form()
}







lister_tag()


document.querySelector(".add_tag #couleur_fond").addEventListener("change", ()=>{
    let form = document.querySelector(".add_tag form")
    document.querySelector(".add_tag .test").style.backgroundColor = form['couleur_fond'].value
})
document.querySelector(".add_tag #couleur_texte").addEventListener("change", ()=>{
    let form = document.querySelector(".add_tag form")
    document.querySelector(".add_tag .text_test").style.color = form['couleur_texte'].value
})