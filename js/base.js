window.addEventListener("click", (e) => {
    let bar = document.querySelector(".search_input")
    if(e.target != bar.children[1]){
        bar.style.border = "1px solid transparent"
        bar.style.backgroundColor = "#F6FBFF"
        bar.children[1].style.width = "200px"
    }
})

function search_bar_action(){
    let bar = document.querySelector(".search_input")
    
    bar.style.border = "1px solid #93d0ff"
    bar.style.backgroundColor = "#d9eeff"
    bar.children[1].style.width = "400px"
}


function changer_en_grille(){
    let grid_btn = document.querySelector(".grille_btn")
    let list_btn = document.querySelector(".list_btn")
    let box = document.querySelector(".liste_box")

    list_btn.style.color = "grey"
        grid_btn.style.color = "#0070C1"
        box.setAttribute("class", "liste_box grille")
}


function changer_en_liste(){
    let grid_btn = document.querySelector(".grille_btn")
    let list_btn = document.querySelector(".list_btn")
    let box = document.querySelector(".liste_box")

    list_btn.addEventListener("click", ()=>{
        list_btn.style.color = "#0070C1"
        grid_btn.style.color = "grey"
        box.setAttribute("class", "liste_box liste")
    })
}




