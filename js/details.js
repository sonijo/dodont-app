function afficher_alert(error){
    error = document.querySelector(error)
    error.style.top = '8%'
}

function close_alert(error){
    setTimeout(() => {
        error = document.querySelector(error)
        error.style.top = '-25%'
    }, 2000)
    
}


function affiche_detail(id){
    let tete = document.querySelector(".entete_haut")
    let detail = document.querySelector(".detail")
    let list = document.querySelector(".liste_box")

    list.setAttribute('class', 'liste_box grille')
    list.setAttribute("style", "width: 30%;"+
        "display: flex;"+
        "flex-direction: column"
    )
    
    tete.style.height = "0px"

    detail.setAttribute("style", ""+
        "background-color: white;"+
        "border-radius: 10px;"+
        "padding: 20px;"+
        "width: 60%;"+
        "border: 1px solid rgba(0, 0, 0, 0.1);"
    )

    let tache = JSON.parse(localStorage.getItem('tache')).data
    if(tache){
        
    }
    
    document.querySelector(".date-infos").textContent = tache.date
    
    if(tache.date_rappel != null || tache.date_rappel != ""){
        document.querySelector('.rappel-infos').style.display = "initial"
        document.querySelector('.detail .line_3').style.display = "initial"
        document.querySelector(".date-rap").textContent = tache.date_rappel
        document.querySelector(".heure-rap").textContent = tache.heure_rappel


        tab_date = tache.date_rappel.split('-')
        tab_heure = tache.heure_rappel.split(":")
        tab_date.forEach((elt, i) => tab_date[i] = parseInt(elt))
        tab_heure.forEach((elt, i) => tab_heure[i] = parseInt(elt))

        let yy = tab_date[0] - new Date().getFullYear()
        let mm = tab_date[1] - new Date().getMonth()
        let jj = tab_date[2] - new Date().getDate()

        let days = (yy * 365) + (mm * 30) + jj
        let hours = new Date().getHours() > tab_heure[0] ? (new Date().getHours() - tab_heure[0]) : (new Date().getHours() - tab_heure[0]) * -1
        let minutes = new Date().getMinutes() > tab_heure[0] ? (new Date().getMinutes() - tab_heure[0]) : (new Date().getHours() - tab_heure[0]) * -1
        compteur(days, hours, minutes)
    }
    document.querySelector(".titre-gros").textContent = tache.titre
    document.querySelector(".line_2 p").textContent = tache.contenu
}

function cache_detail(){
    let tete = document.querySelector(".entete_haut")
    let detail = document.querySelector(".detail")
    let list = document.querySelector(".liste_box")

    if(localStorage.getItem("liste") == false){
        list.setAttribute('class', 'liste_box grille')
        list.style.gridTemplateColumns = "repeat(3, 1fr)"
    }
    else{
        list.setAttribute('class', 'liste_box liste')
        list.style.gridTemplateColumns = "repeat(1, 1fr)"
    }
    
    list.style.width = "100%"
    tete.style.height = "250px"

    detail.setAttribute("style", ""+
        "background-color: transparent;"+
        "border-radius: 0px;"+
        "padding: 0px;"+
        "width: 0%;"+
        "border: none;"
    )
    setTimeout(() => location.reload(), 300)
}

function add_modification_data(){
    let tache = JSON.parse(localStorage.getItem('tache')).data

    let form = document.querySelector('.modifier_tache form')
    form['titre'].value = tache.titre
    form['contenu'].value = tache.contenu
    form['date'].value = tache.date_rappel
    form['heure'].value = tache.heure_rappel
}


function effectuer(id){
    let row = document.getElementById("row_" + id)

    row.setAttribute("style", "text-decoration: line-through;"+
        "color: rgb(201, 201, 201);"
    )

    if(document.querySelector(".grille")){
        setTimeout(() => {
            row.style.overflow = "hidden"
            row.style.width = "0%"
            row.style.padding = "0px"
            row.style.border = "none"
            row.style.boxShadow = "none"
            row.style.marginBottom = "0px"
        }, 700)
    }else{
        setTimeout(() => {
            row.style.overflow = "hidden"
            row.style.height = "0px"
            row.style.padding = "0px"
            row.style.border = "none"
            row.style.boxShadow = "none"
            row.style.marginBottom = "0px"
        }, 700)
    }

    afficher_alert(".tache_effectuee")
    close_alert(".tache_effectuee")
    
    setTimeout(() => {
        row.style.display = "none"
    }, 1500)
    
}


function supprimer(){
    let id = JSON.parse(localStorage.getItem("tache")).key
    let row = document.getElementById("row_" + id)

    row.setAttribute("style", "background-color: #FFDEDE; color: red;")
    if(document.querySelector(".grille")){
        setTimeout(() => {
            row.style.overflow = "hidden"
            row.style.width = "0%"
            row.style.padding = "0px"
            row.style.border = "none"
            row.style.boxShadow = "none"
            row.style.marginBottom = "0px"
        }, 700)
    }else{
        setTimeout(() => {
            row.style.overflow = "hidden"
            row.style.height = "0px"
            row.style.padding = "0px"
            row.style.border = "none"
            row.style.boxShadow = "none"
            row.style.marginBottom = "0px"
        }, 700)
    }
    close_form()

    setTimeout(() => {
        afficher_alert(".tache_supprimee")
    }, 300)
    
    setTimeout(() => {
        row.style.display = "none"
    }, 1500)
}




function open_form(form){
    let bg = document.querySelector(".bg_flou")
    form = document.querySelector(form)

    bg.setAttribute("style", "display: initial")
    form.style.scale = 1
    form.style.top = "50%"
    form.style.left = "50%"
}


function close_form(){
    let bg = document.querySelector(".bg_flou")
    let boxes = document.querySelectorAll(".box-form")
    bg.setAttribute("style", "display: none")
    for(let i=0; i< boxes.length; i++){
        boxes[i].style.scale = 0.1
        boxes[i].style.top = "-40%"
        boxes[i].style.left = "40%"
    }
}






function show_select_filter(){
    let select_filter = document.querySelector(".list-choix")
    select_filter.style.opacity = 1
}

function ajouter_filtre(filtre){
    let filtres = document.querySelector(".filtres")
    let span = document.createElement("span")
    let effacer_btn = document.querySelector(".drop")
    
    filtres.removeChild(effacer_btn)

    span.setAttribute("class", "one_filter")
    span.innerHTML = `${filtre} <i class="fa-solid fa-xmark"></i>`
    filtres.appendChild(span)
    filtres.appendChild(effacer_btn)

    let select_filter = document.querySelector(".list-choix")
    select_filter.style.opacity = 0
}


function effacer_tout(){
    document.querySelector(".filtres").innerHTML = ""
}



function afficher_resultat_recherche(){
    document.querySelector(".search_results").style.display = "flex"
}

function close_resultats(id){
    document.querySelector(".search_results").style.display = "none"
    let input = document.querySelector(".search_input input")
    let span_result = document.querySelectorAll(".search_results span")

    input.value = span_result[id].textContent
}

function choose_result(){
    document.querySelector(".search_input input").value = this.textContent
}



function compteur(days, hours, minutes){
    var seconds = 0;
    var startCountdown = 0;
    var myTimerSpeed = 10;
    var myTimer;
    var countdown = 0;
    days = 1

    // Pour le début du chrono
    clearTimer()
    start_timer()

    function start_timer(){
        startCountdown = setTime()
        countdown = startCountdown

        myTimer = setInterval(timerTick, myTimerSpeed)
    }

    function timerTick(){
        countdown--

        days = Math.floor(countdown / (100 * 60 * 60 * 24));
        hours = Math.floor((countdown % (100 * 60 * 60 * 24)) / (100 * 60 * 60));
        minutes = Math.floor((countdown % (100 * 60 * 60)) / (100 * 60));
        seconds = Math.floor((countdown % (100 * 60)) / 100);

        console.log(days)
        document.querySelector(".jours").textContent = (days < 0) ? 0 : days
        document.querySelector(".heures").textContent = (hours < 0) ? 0 : hours
        document.querySelector(".minutes").textContent = (minutes < 0) ? 0 : minutes
        document.querySelector(".secondes").textContent = (seconds < 0) ? 0 : seconds

        // console.log('countdown = ' + parseInt(countdown/100))

        if(countdown <= 0){
            clearTimer()
        }
    }


    function setTime(){
        seconds = 0

        return ((days*86400)+(hours*3600)+(minutes*60)+seconds)*100;
    }

    function clearTimer(){
        clearInterval(myTimer)
    }
}




localStorage.setItem("liste", false)


close




window.addEventListener("click", (e) => {
    if(e.target.tagName != "SPAN")
        document.querySelector(".search_results").style.opacity = 0
})

