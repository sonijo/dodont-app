function lister(){
    let tag = {}                
    let nb = 0
    let taches = new Array(0)
    let liste_box = document.querySelector(".liste_box")
    liste_box.innerHTML = ""
    let idb = indexedDB.open("crud", 1)
    idb.onsuccess= ()=> {
        let res = idb.result
        let tx = res.transaction("tache", "readonly")
        let store = tx.objectStore("tache")
        let cursor = store.openCursor()

        cursor.onsuccess = (e) => {
            let curRes = e.target.result

            if(curRes){
                if(curRes.value.effectue == 1){
                    let div = document.createElement("div")
                    div.setAttribute("class", "row")
                    div.setAttribute("id", "row_" + curRes.key)
                    div.innerHTML = `
                        <!-- <div class="check">
                            <input type="checkbox">
                        </div> -->
    
                        <div class="part_1"  onclick="affiche_detail(${curRes.key}); read_one_tache(${curRes.key});">
                            <div class="titre_tache">
                                <h6>${curRes.value.titre} <span class="tag">Ecole</span></h6>
                                <span class="infos_date">
                                ${curRes.value.date_rappel ? `
                                    <span class="rappel">
                                        <i class="fa-solid fa-circle-exclamation"></i>
                                        <span class="date_rappel">${curRes.value.date_rappel}</span> |
                                        <span class="heure_rappel">${curRes.value.heure_rappel}</span> 
                                    </span>` : `` }
                                    <span class="date">${curRes.value.date}</span>
                                </span>
                            </div>
    
                            <p>${curRes.value.contenu}</p>
                            
                        </div>
    
                        <div class="opt">
                            <span class="i valider" onclick="effectuer(${curRes.key}); read_one_tache(${curRes.key}); effectuer_tache()">
                                <i class="fa-solid fa-check"></i>
                            </span>
                            <span class="i modifier" onclick="open_form('.modifier_tache'); read_one_tache(${curRes.key}); add_modification_data()">
                                <i class="fa-regular fa-pen-to-square"></i>
                            </span>
                            <span class="i supprimer" onclick="open_form('.supprimer_tache'); read_one_tache(${curRes.key})">
                                <i class="fa-solid fa-trash-can"></i>
                            </span>
                            
                        </div>
                    `
                    liste_box.append(div)
                }

                curRes.continue()
            }
        }
        localStorage.setItem('taches', JSON.stringify(taches))
    }
}

lister()


var buttons = document.getElementsByClassName("btn btn-outline-primary bouton_add");
for(var i = 0; i < buttons.length; i++){
    buttons[i].addEventListener('click', function() {
        var sound = document.getElementById("mySound");
        sound.play();
    });
}

var links = document.getElementsByClassName("a-solid fa-list-check");
for(var i = 0; i < links.length; i++){
    links[i].addEventListener('click', function(event) {
        event.preventDefault(); // Empêche le lien de suivre l'URL
        var sound = document.getElementById("mySound");
        sound.play();
    });
}
